======
pyehik
======


.. image:: https://img.shields.io/pypi/v/pyehik.svg
        :target: https://pypi.python.org/pypi/pyehik

.. image:: https://img.shields.io/travis/bit4bit/pyehik.svg
        :target: https://travis-ci.org/bit4bit/pyehik

.. image:: https://readthedocs.org/projects/pyehik/badge/?version=latest
        :target: https://pyehik.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status



* Free software: MIT license
* Documentation: https://pyehik.readthedocs.io.
* Source Code: https://efossils.somxslibres.net/fossil/user/bit4bit/repository/pyehik/index

Features
--------

* A lot functiones implemented.

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

